<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'db_sp' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ',-XH_zrIHL3fFM:kMUCCr$NG`:5:^|SOxs~r>,#DkQ1E+|<j&/<(Jis%w:!*H%V!');
define('SECURE_AUTH_KEY',  'h9p(QzA9-8FmICTJI)w;Jb,+,_%1|Xm~GR}v6ztYS(fiyV-Bw@k DCzR#}gf7}lZ');
define('LOGGED_IN_KEY',    'efW=rGdKOMpX|Qf0b7q:AyJ+HSk/FEv/Mp+#s%P)^Gt|WE/sm3E#>n/L(a1?0Z1R');
define('NONCE_KEY',        'k:m2_f 8u[e$dxfJmtXyG4!:1Jz;o@xxgr?FWa}piGK[Hs3QAZJ6nw)X;4N1Dt-+');
define('AUTH_SALT',        '>FCL}Q:c,;U8OD_bzl6Q`CyC[!=2Zi(_&C{T>UMUUi,k}x<S@+^1.|l`1|.ZS-*=');
define('SECURE_AUTH_SALT', ']go6:W>TQXsI?{#H9;~tWiiOFk=$NE#pXborv*$(*Gl&e>~l{lup+sSOo[p*>.@g');
define('LOGGED_IN_SALT',   'c}HKp#6FU!]s|Z+ox& o^83+exL-(3XDQ9x(x1/`P{gbgpl)BI0}5UPWVfQ3%A-|');
define('NONCE_SALT',       '_OP@3}2mSpa+;f,v67]$ox>!rT%fd#R]&o-&|1:-)n-*FUm4%V72BBrOA*Jl$TeZ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wpsp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
