<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package TA Meghna
 */

if ( ! is_active_sidebar( 'sidebar-right' ) ) {
	return;
}
?>
			
<?php dynamic_sidebar( 'sidebar-right' ); ?>
		



