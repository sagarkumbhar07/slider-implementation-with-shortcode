<?php
/**
 * The template for displaying search results pages.
 *
 * @package TA Meghna
 */

get_header();



get_template_part('breadcrumb');
 ?>


	<!-- Blog banner -->
	

	<section id="blog-page">
		<div class="container">
			<div class="row">
				<div id="blog-posts" class="col-md-8 col-sm-12">
					<main id="main" class="site-main" role="main">

					<div class="post-item">
					<?php if ( have_posts() ) : ?>

							<?php /* Start the Loop */ ?>
							<?php while ( have_posts() ) : the_post(); ?>

								<?php
								/**
								 * Run the loop for the search to output the results.
								 * If you want to overload this in a child theme then include a file
								 * called content-search.php and that will be used instead.
								 */
								get_template_part( 'content', 'search' );
								?>

							<?php endwhile; ?>

							<?php ta_pagination(); ?>

						<?php else : ?>

							<?php get_template_part( 'content', 'none' ); ?>

						<?php endif; ?>
					</div><!-- .post-item -->

					</main><!-- #main -->
				</div><!-- #blog-posts -->
                <!-- Widget section -->
			    <div id="right-sidebar" class="col-md-4 col-sm-12 widget-area" role="complementary">
                    <?php get_template_part('sidebar-category');?>
                    <?php  dynamic_sidebar( 'reports-sidebar' ); ?>
                </div>
            </div><!-- #right-sidebar -->
        </div>
    </section>
<?php get_footer(); ?>







