<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 */
?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7">
<![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8">
<![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9">
<![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php $fav = ta_option( 'custom_favicon', false, 'url' ); ?>
<?php if ( $fav !== '' ) : ?>
<link rel="icon" type="image/png" href="<?php echo ta_option( 'custom_favicon', false, 'url' ); ?>" />
<?php endif; ?>

<!-- Favicon -->
<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri();?>/images/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri();?>/images/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri();?>/images/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri();?>/images/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri();?>/images/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri();?>/images/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri();?>/images/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri();?>/images/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri();?>/images/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_template_directory_uri();?>/images/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri();?>/images/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri();?>/images/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri();?>/images/favicon/favicon-16x16.png">
<link rel="manifest" href="<?php echo get_template_directory_uri();?>/images/favicon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri();?>/images/favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
<!--/ Favicon -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<?php wp_head(); ?>
<meta name="google-site-verification" content="X8SJitYepJJK7dw_8tpuYCK3Z-tXDrDt9HnvJKqnH0E" />
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.css">
</head>
    <!--
<script language=JavaScript> var message="Function Disabled!"; function clickIE4(){ if (event.button==2){ alert(message); return false; } } function clickNS4(e){ if (document.layers||document.getElementById&&!document.all){ if (e.which==2||e.which==3){ alert(message); return false; } } } if (document.layers){ document.captureEvents(Event.MOUSEDOWN); document.onmousedown=clickNS4; } else if (document.all&&!document.getElementById){ document.onmousedown=clickIE4; } document.oncontextmenu=new Function("alert(message);return false") </script>
ondragstart="return false" onselectstart="return false"
-->
<body <?php body_class(); ?> >


<div id="page" class="hfeed site">

	<div id="content" class="site-content">

    <!-- / START TOP BAR -->
    <header id="navigation" role="banner">
        <nav class="navbar navbar-fixed-top" role="navigation" id="sticky">
            <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
                 <div class="navbar-header">
                  <?php if ( has_nav_menu( 'primary' ) || has_nav_menu( 'secondary' ) ) { ?>
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse" id="hamburger">
                    <span class="glyphicon glyphicon-menu-hamburger"></span>
                  </button>
                  <?php } ?>
                  <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" style="font-family: 'Oswald', sans-serif;color: #fff;/* font-size: 38px; */ /* font-weight: bold; */">
                  <div class="logo" id="logo-sticky">
                   
                  </div>
                </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                         <?php if ( has_nav_menu( 'primary' ) || has_nav_menu( 'secondary' ) ) { ?>
                             <div class="collapse navbar-collapse navbar-ex1-collapse">
                                   <?php if ( is_front_page() ) {
                                          $args = array(
                                              'theme_location' => 'primary',
                                              'depth'          => 3,
                                              'container'      => false,
                                              'menu_id'        => 'sticky-navbar-nav',
                                              'menu_class'     => 'nav navbar-nav navbar-right',
                                              'walker'         => new wp_bootstrap_navwalker()
                                          );

                                          if ( has_nav_menu( 'primary' ) ) {
                                              wp_nav_menu( $args );
                                          }
                                      } else {
                                          $args = array(
                                              'theme_location' => 'secondary',
                                              'depth'          => 3,
                                              'container'      => false,
                                              'menu_id'        => 'sticky-navbar-nav',
                                              'menu_class'     => 'nav navbar-nav navbar-right',
                                              'walker'         => new wp_bootstrap_navwalker()
                                          );

                                          if ( has_nav_menu( 'secondary' ) ) {
                                              wp_nav_menu( $args );
                                          }
                                      } ?> 
                                </div><!-- /.navbar-collapse -->
                            <?php } ?>
                
               <!--  <div class="header-menu" id="sticky">
                  
               </div> -->
              </div> 

       </nav>
      
    </header>