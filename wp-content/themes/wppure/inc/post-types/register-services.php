<?php
/**
 * WP Custom Post Type Class
 *
 * @package TA Meghna
 */

$services = new CPT(
	array(
		'post_type_name' => 'courses',
		'singular'       => __( 'Course', 'ta-meghna' ),
		'plural'         => __( 'Courses', 'ta-meghna' ),
		'slug'           => 'course'
	),

	array(
		'supports'  => array( 'title', 'editor', 'thumbnail', 'excerpt', 'custom-fields', 'comments' ),
		'menu_icon' => 'dashicons-portfolio'
	)
);

$services -> register_taxonomy(


    array(
		'taxonomy_name' => 'courses_categories',
		'singular'      => __( 'Courses Category', 'ta-meghna' ),
		'plural'        => __( 'Courses Categories', 'ta-meghna' ),
		'slug'          => 'courses-category'	
				  )

);
$services -> register_taxonomy(
	array(
		'taxonomy_name' => 'courses_tags',
		'singular'      => __( 'Courses Tag', 'ta-meghna' ),
		'plural'        => __( 'Courses Tags', 'ta-meghna' ),
		'slug'          => 'courses-tag'
	)
);








