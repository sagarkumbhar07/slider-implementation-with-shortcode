<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package TA Meghna
 */
?>


  </div><!-- #content -->

  <footer id="footer" class="bg-white">
    <div class="container">
      <div class="row" data-wow-duration="500ms">
        <div class="col-lg-6 col-xs-12 col-md-6">
            <div class="copyright">
                   <p>dstravelsupplies &copy; <?php echo date('Y');?> <br>
            </div>
        </div>
        <div class="col-lg-6 col-xs-12 col-md-6">
            <ul class="link-follow">
                <li><a class="facebook fa fa-instagram" href="#"></a></li>
                <li><a class="facebook fa fa-twitter" href="#"></a></li>
                <li><a class="facebook fa fa-facebook" href="#"></a></li>
                <li><a class="facebook fa fa-linkedin" href="#"></a></li>
            </ul>
        </div>
      </div> <!-- .row -->
    </div> <!-- .container -->
  </footer>
    <!-- Copyright End -->

</div><!-- #page -->

<!-- Back to top -->
<a href="#" id="scrollUp">
  <i class="fa fa-level-up fa-2x"></i>
</a>
<!-- End back to top -->

<?php wp_footer(); ?>
</body>
</html>