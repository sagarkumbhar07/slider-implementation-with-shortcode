<?php
/**
 * Template Name: Front Page Made By Sagar
 *
 * @package TA Meghna
 */

get_header(); 
?>

<section class="clients-section">

    <?php echo do_shortcode('[sagarwpslider]'); ?>      

</section>

<section class="intro-section" style="background-image: url(https://img.freepik.com/free-vector/white-background-with-blue-tech-hexagon_1017-19366.jpg?size=626&ext=jpg);">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="intro-content">
					<p>Founded ib 1969, D&S Travel Supplies has grown to become a key manufacturer of high-quality branded travel merchandies trusted by customers worldwide. Our expert team designs and produce tailer-made travel gifts and travel supplies ranging from small promotional items to complete sets ofbranded products.</p>
					<a href="#">About Us <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
				</div> 
			</div>
		</div>
	</div>
</section>

<section class="our-product-section">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="title text-center wow fadeInDown animated">
					<h2>Our Products</h2>
                </div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4">
				<div class="our-product-content">
					<img src="https://img1.exportersindia.com/product_images/bc-full/2019/12/4145221/trolley-suitcase-1577180646-5220358.jpeg">
					<h3>A trolly suitcase</h3>
				</div> 
			</div>
			<div class="col-lg-8">
				<div class="row">
					<div class="col-lg-6">
						<div class="our-product-content">
							<img src="https://www.anyahindmarch.com/on/demandware.static/-/Library-Sites-anya-shared-library/default/dw61b7183d/images/Landing/Landing-Page_INAPB-03.jpg">
							<h3>A trolly suitcase</h3>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="our-product-content">
							<img src="https://www.anyahindmarch.com/on/demandware.static/-/Library-Sites-anya-shared-library/default/dw61b7183d/images/Landing/Landing-Page_INAPB-03.jpg">
							<h3>Pen Gift Set</h3>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6">
						<div class="our-product-content">
							<img src="https://www.anyahindmarch.com/on/demandware.static/-/Library-Sites-anya-shared-library/default/dw61b7183d/images/Landing/Landing-Page_INAPB-03.jpg">
							<h3>USB Pen</h3>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="our-product-content">
							<img src="https://www.anyahindmarch.com/on/demandware.static/-/Library-Sites-anya-shared-library/default/dw61b7183d/images/Landing/Landing-Page_INAPB-03.jpg">
							<h3>Selfi Stick</h3>
						</div>
					</div>
				</div>
			</div> 
		</div>
		<div class="row">
			<div class="col-lg-8">
				<div class="row">
					<div class="col-lg-6">
						<div class="our-product-content">
							<img src="https://www.anyahindmarch.com/on/demandware.static/-/Library-Sites-anya-shared-library/default/dw61b7183d/images/Landing/Landing-Page_INAPB-03.jpg">
							<h3>Sun Glasses</h3>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="our-product-content">
							<img src="https://www.anyahindmarch.com/on/demandware.static/-/Library-Sites-anya-shared-library/default/dw61b7183d/images/Landing/Landing-Page_INAPB-03.jpg">
							<h3>Transparent Rucksack</h3>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6">
						<div class="our-product-content">
							<img src="https://www.anyahindmarch.com/on/demandware.static/-/Library-Sites-anya-shared-library/default/dw61b7183d/images/Landing/Landing-Page_INAPB-03.jpg">
							<h3>Umbrella</h3>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="our-product-content">
							<img src="https://www.anyahindmarch.com/on/demandware.static/-/Library-Sites-anya-shared-library/default/dw61b7183d/images/Landing/Landing-Page_INAPB-03.jpg">
							<h3>Binaculars</h3>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="our-product-content">
					<img src="https://img1.exportersindia.com/product_images/bc-full/2019/12/4145221/trolley-suitcase-1577180646-5220358.jpeg">
					<h3>Water Bottel</h3>
				</div> 
			</div> 
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="view-more">
					<a href="#">View More</a>
                </div>
			</div>
		</div>
	</div>
</section>


<section class="are-you-together-section" style="background-image: url(https://cdn7.bigcommerce.com/s-w3u6iwh84c/product_images/uploaded_images/duffel-group-banner-img-9231.jpg);">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-lg-offset-6">
				<div class="are-you-together-section-content intro-content">
					<h3>ARE YOU READY TO WORK TOGETHER</h3>
					<p>Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts</p>
					<a href="#">Contact Us <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="our-feature-section">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="title text-center wow fadeInDown animated">
					<h2>Our Feature</h2>
                </div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<div class="feature-content-brown feature-content-left">
					<div class="row">
						<div class="col-lg-6">
							<div class="feature-content-inner">
								<h3>Small Order</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua.</p>
								<a href="#">View More</a>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="feature-content-inner">
								<img src="https://www.freepngimg.com/thumb/chair/83928-sims-textile-pillow-dog-throw-download-hd-png.png">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="feature-content-gray feature-content-left">
					<div class="row">
						<div class="col-lg-6">
							<div class="feature-content-inner">
								<h3>Ecofriendly</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua.</p>
								<a href="#">View More</a>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="feature-content-inner">
								<img src="https://www.freepngimg.com/thumb/chair/83928-sims-textile-pillow-dog-throw-download-hd-png.png">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<div class="feature-content-gray feature-content-left">
					<div class="row">
						<div class="col-lg-6">
							<div class="feature-content-inner">
								<img src="https://www.freepngimg.com/thumb/chair/83928-sims-textile-pillow-dog-throw-download-hd-png.png">
							</div>
						</div>
						<div class="col-lg-6">
							<div class="feature-content-inner">
								<h3>Retails Gifts</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua.</p>
								<a href="#">View More</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="feature-content-blue feature-content-left">
					<div class="row">
						<div class="col-lg-6">
							<div class="feature-content-inner">
								<img src="https://www.freepngimg.com/thumb/chair/83928-sims-textile-pillow-dog-throw-download-hd-png.png">
							</div>
						</div>
						<div class="col-lg-6">
							<div class="feature-content-inner">
								<h3>Cup Cakes</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua.</p>
								<a href="#">View More</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>
