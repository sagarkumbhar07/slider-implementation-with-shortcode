<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package TA Meghna
 */

get_header(); 
?>

<section id="blog-banner">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center">
					<div class="blog-title">
					  	<?php the_archive_title( '<h1 class="page-title">', '</h1>' ); ?>
					</div>
				</div><!-- .col-lg-12 -->
			</div><!-- .row -->
		</div><!-- .container -->
	</section>

<?php get_template_part('breadcrumb');?>

	<section id="blog-page">
		<div class="container">
			<div class="row">
				<div id="blog-posts" class="col-md-8 col-sm-12">
					<main id="main" class="site-main" role="main">

					<div class="post-item">
					<?php if ( have_posts() ) : ?>

						<?php /* Start the Loop */ ?>
						<?php while ( have_posts() ) : the_post(); ?>

							<?php
								/* Include the Post-Format-specific template for the content.
								 * If you want to override this in a child theme, then include a file
								 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
								 */
								get_template_part( 'content', get_post_format() );
							?>

						<?php endwhile; ?>

						<?php ta_pagination(); ?>

					<?php else : ?>

						<?php get_template_part( 'content', 'none' ); ?>

					<?php endif; ?>
					</div><!-- .post-item -->

					</main><!-- #main -->
				</div><!-- #blog-posts -->
                     <!-- Widget section -->
			    <div id="right-sidebar" class="col-md-4 col-sm-12 widget-area" role="complementary">
                    <?php  get_template_part('sidebar-category');?>
                    <?php // get_sidebar(); ?>
                </div>
            </div><!-- #right-sidebar -->
        </div>
    </section>
<?php get_footer(); ?>