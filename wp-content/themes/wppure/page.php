<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package TA Meghna
 */

get_header();

get_template_part('breadcrumb');
 ?>




<section id="blog-page">
	<div class="container">
		<div class="row">
			<div id="blog-posts" class="col-md-8 col-sm-12">
				<main id="main" class="site-main" role="main">
					<div class="post-item">
					<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'content', 'page' ); ?>

						<!-- <?php
							// If comments are open or we have at least one comment, load up the comment template
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;
						?> -->

					<?php endwhile; // end of the loop. ?>
					</div><!-- .post-item -->

				</main><!-- #main -->
			</div><!-- #blog-posts -->
           <div id="right-sidebar" class="col-md-4 col-sm-12 widget-area" role="complementary">
               <!--  <?php get_template_part('sidebar-category');?> -->
                <?php  dynamic_sidebar( 'reports-sidebar' ); ?>
            </div>            
        </div>
	</div>
</section>
<!-- End blog post section -->
<?php get_footer(); ?>
