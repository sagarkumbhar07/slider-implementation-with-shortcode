<?php
/**
 * TA Meghna functions and definitions
 *
 * @package TA Meghna
 */

/*
 * Make theme available for translation.
 * Translations can be filed in the /languages/ directory.
 * If you're building a theme based on TA Meghna, use a find and replace
 * to change 'ta-meghna' to the name of your theme in all the template files
 */
load_theme_textdomain( 'ta-meghna', get_template_directory() . '/languages' );

 /**
 * Include the Redux theme options Framework.
 */
if ( !class_exists( 'ReduxFramework' ) ) {
	require_once( get_template_directory() . '/redux/framework.php' );
}

/**
 * Register all the theme options.
 */
require_once( get_template_directory() . '/inc/redux-config.php' );

/**
 * Theme options functions.
 */

require_once( get_template_directory() . '/inc/ta-option.php' );

/**
 * Set the content width based on the theme's design and stylesheet.
 */

// require_once( get_template_directory() . '/inquiry-template.php' );



if ( ! isset( $content_width ) ) {
	$content_width = 750; /* pixels */
}

/**
 * Set the content width for full width pages with no sidebar.
 */
function full_width_page() {
	if ( is_page_template( 'template-full-width-page.php' ) ) {
		global $content_width;
		$content_width = 1140; /* pixels */
	}
}
add_action('template_redirect', 'full_width_page');


// GET POST FEATURED IMAGE
// function MT_get_featured_image($post_ID) {
//     $post_thumbnail_id = get_post_thumbnail_id($post_ID);
//     if ($post_thumbnail_id) {
//         $post_thumbnail_img = wp_get_attachment_image_src($post_thumbnail_id, 'featured_preview');
//         return $post_thumbnail_img[0];
//     }
// }

// ADD NEW POST COLUMN
function MT_columns_head($defaults) {
    $defaults['featured_image'] = 'Image';
    return $defaults;
}

// SHOW THE POST FEATURED IMAGE COLUMN
function MT_columns_content($column_name, $post_ID) {
    if ($column_name == 'featured_image') {
        $post_featured_image = MT_get_featured_image($post_ID);
        if ($post_featured_image) {
            // HAS A FEATURED IMAGE
            echo '<img style="widht:50px;height:50px;" src="' . $post_featured_image . '" />';
        }
        else {
            // NO FEATURED IMAGE, SHOW THE DEFAULT ONE
            echo '<img style="widht:50px;heigth:50px;" src="' . get_template_directory_uri() . '/images/missing_56.png" />';
        }
    }
}

// ALL POST TYPES: posts AND custom post types
add_filter('manage_posts_columns', 'MT_columns_head');
add_action('manage_posts_custom_column', 'MT_columns_content', 10, 2);





if ( ! function_exists( 'ta_meghna_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function ta_meghna_setup() {

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'ta-meghna' ),
		'secondary' => __( 'Secondary Menu', 'ta-meghna' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
//	add_theme_support( 'post-formats', array('gallery', 'audio', 'video',) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'ta_meghna_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // ta_meghna_setup
add_action( 'after_setup_theme', 'ta_meghna_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function ta_meghna_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'ta-meghna' ),
		'id'            => 'sidebar-right',
		'description'   => __( 'Main sidebar that appears on the right.', 'ta-meghna' ),
		'before_widget' => '<aside id="%1$s" class="panel panel-default widget %2$s wow fadeIn">',
		'after_widget'  => '</aside>',
		'before_title'  => '<div class="panel-heading"><h3>',
		'after_title'   => '</h3></div><div class="panel-content">',
	) );

    register_sidebar( array(
		'name'          => __( 'Reports Sidebar', 'ta-meghna' ),
		'id'            => 'reports-sidebar',
		'description'   => __( 'Main sidebar that appears on the right.', 'ta-meghna' ),
		'before_widget' => '<aside id="%1$s" class="panel panel-default widget %2$s wow fadeIn">',
		'after_widget'  => '</aside>',
		'before_title'  => '<div class="panel-heading"><h3>',
		'after_title'   => '</h3></div><div class="panel-content">',
	) );

    register_sidebar( array(
		'name'          => __( 'Sidebar Footer 1', 'ta-footer-1' ),
		'id'            => 'sidebar-footer-1',
		'description'   => __( 'Main sidebar that appears on the right.', 'ta-meghna' ),
		'before_widget' => '<aside id="%1$s" class="widget footer-widget %2$s wow fadeIn">',
		'after_widget'  => '</aside>',
		'before_title'  => '<div class="headline footer-headline wow fadeIn animated animated" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-name: fadeIn;"><h3>',
		'after_title'   => '</h3></div>',
	) );

     register_sidebar( array(
		'name'          => __( 'Sidebar footer 2', 'ta-footer-2' ),
		'id'            => 'sidebar-footer-2',
		'description'   => __( 'Main sidebar that appears on the right.', 'ta-meghna' ),
		'before_widget' => '<aside id="%1$s" class="widget footer-widget %2$s wow fadeIn">',
		'after_widget'  => '</aside>',
		'before_title'  => '<div class="headline footer-headline wow fadeIn animated animated" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-name: fadeIn;"><h3>',
		'after_title'   => '</h3></div>',
	) );

      register_sidebar( array(
		'name'          => __( 'Sidebar footer 3', 'ta-footer-3' ),
		'id'            => 'sidebar-footer-3',
		'description'   => __( 'Main sidebar that appears on the right.', 'ta-meghna' ),
		'before_widget' => '<aside id="%1$s" class="widget footer-widget %2$s wow fadeIn">',
		'after_widget'  => '</aside>',
		'before_title'  => '<div class="headline footer-headline wow fadeIn animated animated" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-name: fadeIn;"><h3>',
		'after_title'   => '</h3></div>',
	) );

     register_sidebar( array(
		'name'          => __( 'Sidebar footer 4', 'ta-footer-4' ),
		'id'            => 'sidebar-footer-4',
		'description'   => __( 'Main sidebar that appears on the right.', 'ta-meghna' ),
		'before_widget' => '<aside id="%1$s" class=" footer-widget %2$s wow  fadeIn">',
		'after_widget'  => '</aside>',
		'before_title'  => '<div class="headline footer-headline wow fadeIn animated animated" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-name: fadeIn;"><h3>',
		'after_title'   => '</h3></div>',
	) );

	register_widget( 'ta_post_tabs_widget' );
	register_widget( 'ta_mailchimp_widget' );
}
add_action( 'widgets_init', 'ta_meghna_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function ta_meghna_scripts() {
    wp_enqueue_style( 'ta-meghna-base', get_template_directory_uri() . '/layouts/base.css', array(), '4.3.0', 'all' );
    wp_enqueue_style( 'ta-meghna-lightbox-style', get_template_directory_uri() . '/layouts/lightbox.css', array(), '', 'all' );
	wp_enqueue_style( 'ta-meghna-style', get_stylesheet_uri() );
	wp_enqueue_style( 'ta-meghna-responsive-style', get_template_directory_uri() . '/layouts/responsive.css', array(), '', 'all' );

	wp_enqueue_style( 'ta-meghna-animate-style','https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css', array(), '', 'all' );
    wp_enqueue_style( 'GoogleFonts-opensans', 'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800', array(), '', 'all' );

	wp_enqueue_script( 'ta-meghna-modernizr', get_template_directory_uri() . '/js/modernizr.min.js', array(), '2.8.3', true );
	wp_enqueue_script( 'ta-meghna-bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '3.3.4', true );
    wp_enqueue_script( 'ta-meghna-bootstrap-hover', get_template_directory_uri() . '/js/bootstrap-hover.js', array('jquery'), '3.3.4', true );
	wp_enqueue_script( 'ta-meghna-lightbox', get_template_directory_uri() . '/js/jquery.lightbox.js', array('jquery'), '1.1.0', true );
    wp_enqueue_script( 'ta-meghna-owl-carousel', get_template_directory_uri() . '/js/owl.carousel.min.js', array('jquery'), '1.3.3', true );
    wp_enqueue_script( 'ta-meghna-sticky', get_template_directory_uri() . '/js/jquery.sticky.js', array('jquery'), '1.0.0', true );
	wp_enqueue_script( 'ta-meghna-wow', get_template_directory_uri() . '/js/wow.min.js', array(), '1.0.3', true );
	wp_enqueue_script( 'ta-meghna-validate', get_template_directory_uri() . '/js/jquery.validate.min.js', array('jquery'), '0.1', true );
	wp_enqueue_script( 'ta-meghna-app', get_template_directory_uri() . '/js/app.js', array('jquery'), '', true );

	//wp_enqueue_script( 'ta-meghna-utils', get_template_directory_uri() . '/js/utils.js', array('jquery'), '1.1.3', true );
	//wp_enqueue_script( 'ta-meghna-mixitup', get_template_directory_uri() . '/js/jquery.mixitup.min.js', array('jquery'), '2.1.7', true );
	//wp_enqueue_script( 'ta-meghna-nicescroll', get_template_directory_uri() . '/js/jquery.nicescroll.min.js', array('jquery'), '3.6.0', true );
	//wp_enqueue_script( 'ta-meghna-appear', get_template_directory_uri() . '/js/jquery.appear.js', array('jquery'), '', true );
	//wp_enqueue_script( 'ta-meghna-easypiechart', get_template_directory_uri() . '/js/jquery.easypiechart.min.js', array('jquery'), '2.1.6', true );
	//wp_enqueue_script( 'ta-meghna-easing', get_template_directory_uri() . '/js/jquery.easing.js', array('jquery'), '1.3', true );
	//wp_enqueue_script( 'ta-meghna-tweetie', get_template_directory_uri() . '/js/tweetie.min.js', array('jquery'), '', true );
	//wp_enqueue_script( 'ta-meghna-nav', get_template_directory_uri() . '/js/jquery.nav.js', array('jquery'), '3.0.0', true );
    //wp_enqueue_script( 'ta-meghna-countTo', get_template_directory_uri() . '/js/jquery.countTo.js', array('jquery'), '', true );
    //wp_enqueue_script( 'ta-meghna-grid', get_template_directory_uri() . '/js/grid.js', array('jquery'), '', true );
	//$translation_array = array( 'templateUrl' => get_stylesheet_directory_uri(), 'lat' => ta_option( 'google_map_lat' ), 'lon' => ta_option( 'google_map_lon' ) );
	//wp_localize_script( 'ta-meghna-app', 'ta_script_vars', $translation_array );
	//wp_enqueue_script( 'ta-meghna-google-map', 'http://maps.google.com/maps/api/js?sensor=false', array(), '', true );
	//wp_enqueue_script( 'ta-meghna-google-map-customization', get_template_directory_uri() . '/js/google-map.js', array('jquery'), '', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
  //  if (get_post_type() == 'reports') {
         wp_enqueue_style( 'ta-meghna-intlTelInput-css', get_template_directory_uri() . '/layouts/intlTelInput.css', array(), '', 'all' );
         wp_enqueue_script( 'ta-meghna-intlTelInput-js', get_template_directory_uri() . '/js/jquery.intlTelInput.min.js', array('jquery'), '1.3', true );
   // }
}
add_action( 'wp_enqueue_scripts', 'ta_meghna_scripts' );

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Register Custom Navigation Walker.
 */
require_once get_template_directory() . '/inc/wp_bootstrap_navwalker.php';

/**
 * Custom Post Types.
 */
require_once get_template_directory() . '/inc/post-types/CPT.php';


add_action( 'admin_menu', 'my_remove_menu_pages' );
function my_remove_menu_pages() {
    remove_menu_page('theme-options.php');  
}


//function my_custom_fonts() { echo '<style> .toplevel_page_theme_options {display:none !important;} </style>'; }


/**
 * Portfolio Custom Post Type.
 */
//require_once get_template_directory() . '/inc/post-types/register-services.php';

/**
 * Comments Callback.
 */
require_once get_template_directory() . '/inc/comments-callback.php';

/**
 * Add Author Meta.
 */
require_once get_template_directory() . '/inc/author-meta.php';


/**
 * Add custom CSS.
 */
require_once get_template_directory() . '/inc/custom-css.php';
require_once get_template_directory() . '/inc/custom-functions.php';

/**
 * Add Theme Widgets.
 */
require_once ( get_template_directory() . '/widgets/widget-post-tabs.php' );
require_once ( get_template_directory() . '/widgets/widget-mailchimp.php' );
require_once ( get_template_directory() . '/widgets/widgets.php' );
//require_once ( get_template_directory() . '/widgets/report-widgets.php' );

add_action( 'admin_menu', 'admin_remove_menu_pages', 999 );

function admin_remove_menu_pages() {
 // remove_menu_page( 'edit.php' );                   //Posts
 // remove_menu_page( 'upload.php' );                 //Media
 // remove_menu_page( 'edit-comments.php' );          //Comments
 // remove_menu_page( 'themes.php' );                 //Appearance
 // remove_menu_page( 'users.php' );                  //Users
 // remove_menu_page( 'tools.php' );                  //Tools
 // remove_menu_page( 'options-general.php' );        //Settings
 // remove_menu_page( 'admin.php?page=theme_options' );
 // remove_menu_page( 'wpcf7' );
 //remove_submenu_page( 'admin.php', 'theme_options' );
 //remove_submenu_page('themes.php','theme-options.php');
 //remove_submenu_page( 'themes.php', 'ta-options' );
};





function couponxl_get_deepest_taxonomy( $source ){
	$organize_list = array();
	if( sizeof( $source ) == 1 ){
		return array_pop( $source );
	}
	sort_terms_hierarchicaly( $source, $organize_list );
	$organize_list = (array)$organize_list;
	if( !empty( $organize_list ) ){
		$last_item = array_pop( $organize_list );
		if( empty( $last_item->children ) ){
			return $last_item;
		}
		else{
			return couponxl_deepest_taxonomy( $last_item->children );
		}
	}
}

function couponxl_deepest_taxonomy( $children ){
	$last_item = array_pop( $children );
	if( empty( $last_item->children ) ){
		return $last_item;
	}
	else{
		$last_one = couponxl_deepest_taxonomy( $last_item->children );
	}
	return $last_one;
}


function sort_terms_hierarchicaly(Array &$cats, Array &$into, $parentId = 0){
    foreach ($cats as $i => $cat) {
        if ($cat->parent == $parentId) {
            $into[$cat->term_id] = $cat;
            unset($cats[$i]);
        }
    }

    foreach ($into as $topCat) {
        $topCat->children = array();
        sort_terms_hierarchicaly($cats, $topCat->children, $topCat->term_id);
    }
}


function couponxl_append_query_string( $permalink, $include = array(), $exclude = array( 'coupon' ) ){
	global $couponxl_slugs;
	global $wp;
	if ( !$permalink ){
		$permalink = get_permalink();
	}

	// Map endpoint to options
	if ( get_option( 'permalink_structure' ) ) {
		if ( strstr( $permalink, '?' ) ) {
			$query_string = '?' . parse_url( $permalink, PHP_URL_QUERY );
			$permalink    = current( explode( '?', $permalink ) );
		} else {
			$query_string = '';
		}

		$permalink = trailingslashit( $permalink );
		if( !empty( $include ) ){
			foreach( $include as $arg => $value ){
				$permalink .= $couponxl_slugs[$arg].'/'.$value.'/';
			}
		}
		foreach( $couponxl_slugs as $slug => $trans_slug ){
			if( isset( $wp->query_vars[$trans_slug] ) && !isset( $include[$slug] ) && !in_array( $slug, $exclude ) && !in_array( 'all', $exclude ) ){
				$permalink .= $trans_slug.'/'.$wp->query_vars[$trans_slug].'/';
			}
		}
		$permalink .= $query_string;
	} 
	else {
		if( !empty( $include ) ){
			foreach( $include as $arg => $value ){
				$permalink = esc_url( add_query_arg( array( $couponxl_slugs[$arg] => $value ), $permalink ) );
			}
		}
		foreach( $couponxl_slugs as $slug => $trans_slug ){
			if( isset( $wp->query_vars[$trans_slug] ) && !isset( $include[$slug] ) && !in_array( $slug, $exclude ) && !in_array( 'all', $exclude ) ){
				$permalink = esc_url( add_query_arg( array( $trans_slug => $wp->query_vars[$trans_slug] ), $permalink ) );
			}
		}		
		
	}	

	return $permalink;
}

function couponxl_get_organized( $taxonomy ){
	$categories = get_terms( $taxonomy, array('hide_empty' => false));
   // print_r($categories);
	$taxonomy_organized = array();
	sort_terms_hierarchicaly($categories, $taxonomy_organized);
	$taxonomy_organized =  (array) $taxonomy_organized;

	if( $taxonomy == 'reportType' ){
		$sortby = 'name' ;
		$sort = 'asc' ;
	}
	else{
		$sortby = couponxl_get_option( 'all_locations_sortby' );
		$sort = couponxl_get_option( 'all_locations_sort' );
	}


	if( $sort == 'asc' ){
		switch( $sortby ){
			case 'name' : usort( $taxonomy_organized, "couponxl_organized_sort_name_asc" ); break;
			case 'slug' : usort( $taxonomy_organized, "couponxl_organized_sort_slug_asc" ); break;
			case 'count' : usort( $taxonomy_organized, "couponxl_organized_sort_count_asc" ); break;
			default : usort( $taxonomy_organized, "couponxl_organized_sort_name_asc" ); break;
		}
		
	}
	else{
		switch( $sortby ){
			case 'name' : usort( $taxonomy_organized, "couponxl_organized_sort_name_desc" ); break;
			case 'slug' : usort( $taxonomy_organized, "couponxl_organized_sort_slug_desc" ); break;
			case 'count' : usort( $taxonomy_organized, "couponxl_organized_sort_count_desc" ); break;
			default : usort( $taxonomy_organized, "couponxl_organized_sort_name_desc" ); break;
		}
	}

    //print_r($taxonomy_organized);
	return $taxonomy_organized;

}


function couponxl_organized_sort_name_asc( $a, $b ){
    return strcmp( $a->name, $b->name );
}


function couponxl_organized_sort_name_desc( $a, $b ){
    return strcmp( $b->name, $a->name );
}


function couponxl_organized_sort_slug_asc( $a, $b ){
    return strcmp( $a->slug, $b->slug );
}



function couponxl_organized_sort_slug_desc( $a, $b ){
    return strcmp( $b->slug, $a->slug );
}



function couponxl_organized_sort_count_asc( $a, $b ){
    return strcmp( $a->count, $b->count );
}



function couponxl_organized_sort_count_desc( $a, $b ){
    return strcmp( $b->count, $a->count );
}



function couponxl_display_select_tree( $cat, $selected = '', $level = 0 ){
	if( !empty( $cat->children ) ){
		echo '<option value="" disabled>'.str_repeat( '&nbsp;&nbsp;', $level ).''.$cat->name.'</option>';
		$level++;
		foreach( $cat->children as $key => $child ){
			couponxl_display_select_tree( $child, $selected, $level );
		}
	}
	else{
		echo '<option value="'.$cat->term_id.'" '.( $cat->term_id == $selected ? 'selected="selected"' : '' ).'>'.str_repeat( '&nbsp;&nbsp;', $level ).''.$cat->name.'</option>';
	}
}



function couponxl_display_indent_select_tree( $term, $categories, $indent ){
	echo '<option style="padding-left:'.( 10*$indent ).'px;" value="'.esc_attr( $term->slug ).'" '.( in_array( $term->slug, $categories ) ? 'selected="selected"' : '' ).'>'.$term->name.'</option>';	
	if( !empty( $term->children ) ){
		$indent++;
		foreach( $term->children as $key => $child ){
			couponxl_display_indent_select_tree( $child, $categories, $indent );
		}
	}
}



function couponxl_display_tree( $cat, $taxonomy ){
	echo '<ul class="list-unstyled">';
	foreach( $cat->children as $key => $child ){
		echo '<li>
				<a href="'.get_term_link($child->term_id).'">'.$child->name.'</a>
				<span class="count">'.$child->count.'</span>';
				if( !empty( $child->children ) ){
					couponxl_display_tree( $child, $taxonomy );
				}				
		echo '</li>';
	}
    echo '</ul>';

}



function couponxl_ucfirst( $string ){
	$strlen = mb_strlen( $string );
	$firstChar = mb_substr( $string, 0, 1 );
	$then = mb_substr($string, 1, $strlen - 1 );
	return mb_strtoupper( $firstChar ) . $then;
}



add_filter('template_include', 'include_template_function', 1);

function include_template_function($template_path) {

    
    if (get_post_type() == 'reports') {
		if (is_single()) {
			// checks if the file exists in the theme first,
			// otherwise serve the file from the plugin

			if ($theme_file = locate_template(array('single-report.php'))) 
            {

				$template_path = $theme_file;

			} else {

				$template_path = get_template_directory() . '/report-template.php';

			}
		}
        if (is_tax('reportType')) {
			$template_path = locate_template(array('archive-reports.php'));
		}
		

	} elseif (get_post_type() == 'publisher') {
		if (is_single()) {
			// checks if the file exists in the theme first,
			// otherwise serve the file from the plugin
			if ($theme_file = locate_template(array('single-publisher.php'))) {

				$template_path = $theme_file;

			} else {

				$template_path = get_template_directory() . '/publisher-template.php';
			}

		}
	}

	return $template_path;
}


// Modify Slug as per need Wordpress
add_filter( 'sanitize_title', 'wpse52690_limit_length', 1, 3 );

function wpse52690_limit_length( $title, $raw_title, $context ) {
    //  filters
    if( $context != 'save' )
        return $title;

    //  vars
    $desired_length = 70; //number of chars
    $desired_words = 7; //number of words
    $prohibited = array(
        'the'
        ,'in'
        ,'my'
        ,'etc'
        //,'Market'
        //put any more words you do not want to be in the slug in this array
    );

    // filter out unwanted words
    $check_title = explode( ' ', $title );

     // check for particular word and tream url after it
    if (in_array("Market", $check_title))
    {
        list($firstpart)=explode('Market', $title);
        $firstpart = $firstpart.' Market';
        $new_title = $firstpart;
    }
    else
    {
        //  do the actual work
        // filter out unwanted words
        $_title = explode( ' ', $title );
        //if you want more than one switch to preg_split()
        $_title = array_diff( $_title, $prohibited );
    
   
        // count letters and recombine
        $new_title = '';
        for( $i=0, $count=count($_title); $i<$count; $i++ ) {
            //check for number of words
            if( $i > $desired_words )
                break;
            //check for number of letters
            if( mb_strlen( $new_title.' '.$_title[$i] ) > $desired_length )
                break;

            if( $i != 0 )
                $new_title .= ' ';
            $new_title .= $_title[$i];
        }
        
    }

    return $new_title;
}


function productsearchform( $form ) {
 
	$form='<form action="">
      <form role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
      <div class="search-box">
		 <input type="text" placeholder="Explore More" value="' . get_search_query() . '" name="s" id="s" />
         <input type="hidden" value="product" name="post_type" id="s" />
         <button type="submit" class="s-box" id="searchsubmit"><i class="fa fa-search"></i></button>
      </div>
    </form>';
 
    return $form;
}

add_shortcode('productsearch', 'productsearchform');