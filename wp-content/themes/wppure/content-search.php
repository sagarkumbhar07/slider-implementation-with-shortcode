<?php
/**
 * The template part for displaying results in search pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package TA Meghna
 */

?>
<?php  if ( 'post' == get_post_type() ) { ?>
<article id="post-<?php the_ID(); ?>" <?php post_class( array( 'entry' ) ); ?>>
	<div class="post-excerpt">
		<header class="entry-header">
			<?php the_title( sprintf( '<h3 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' ); ?>
		</header><!-- .entry-header -->

		<div class="entry-content">
            <?php the_excerpt();  ?>
		</div><!-- .entry-summary -->
	</div><!-- .post-excerpt -->

	<footer class="entry-footer post-meta">
		<?php ta_meghna_posted_on(); ?>
		<?php ta_meghna_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->

<?php  } elseif ( 'reports' == get_post_type() ) { ?>
<!--<article id="post-<?php the_ID(); ?>" <?php post_class( array( 'report-article', 'wow', 'fadeIn' ) ); ?>  data-wow-duration="1000ms" data-wow-delay="300ms">
    	<div class="report-header">
            <?php the_title( sprintf( '<h3 class="report-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' ); ?>
        </div>

	   <div class="report-content">
        		<p>
                    <?php  
                        $repor_desc = get_post_meta($post->ID, "short_description", true);
                        if(empty($repor_desc))
                        {
                            $repor_desc = get_post_meta($post->ID, "long_description", true);
                                                    
                        }
                        echo trim_characters($repor_desc,'350');
                    ?>
                </p>
                <div class="clearfix">
                    <div class="pull-right"><a class="btn btn-transparent post-link" href="<?php echo get_permalink() ?>" title="<?php the_title(); ?>">Read More</a></div>
                </div>
       </div>
 
	<footer class="report-footer report-meta">
		<ul class="report-list clearfix">
            <li><i class="fa fa-user"></i> <?php echo   get_the_title(get_post_meta($post->ID, "report_publisher", true)); ?></li>
            <li><i class="fa fa-calendar"></i> <?php echo get_post_meta($post->ID, "report_date", true); ?></li>
            <li><i class="fa fa-book"></i> Pages : <?php echo get_post_meta($post->ID, "number_of_pages", true); ?></li>
            <li><i class="fa fa-book"></i> Report ID : <?php echo $post->ID; ?> </li>
            <li><i class="fa fa-dollar"></i> Price for Single User : <?php echo get_post_meta($post->ID, "single_price", true); ?></li>
        </ul>
	</footer> 
</article> -->
                <div id="blog-posts">
                    <main id="main" class="site-main" role="main">
                    <div class="post-item1">
                            <a href="<?php echo get_permalink(); ?>"><h3><?php echo get_the_title(); ?></h3>
                                <div class="rating-report-box">
                                    <span class="label label-warning pull-right"><?php echo get_the_date();?> ago</span>
                                        <div itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">
                                            <span class="hidden">Report ID : <?php echo $post->ID; ?></span>
                                                <?php
                                                $report_rating = get_post_meta($post->ID, "report_rating", true);

                                                //echo $report_rating;
                                                ?>
                                                <span class="rating-value hidden" itemprop="ratingValue" ><?php echo $report_rating;?></span>
                                                <fieldset class="rating">
                                                    <input type="radio" id="star5" name="rating" value="5" <?php if($report_rating == '5') { echo 'checked="" class="check"'; }?>  disabled=""><label for="star5" title="Rocks!"></label>
                                                    <input type="radio" id="star4.5" name="rating" value="4.5" <?php if($report_rating == '4.5') { echo 'checked="" class="check"'; }?> checked="" disabled=""><label for="star4.5" class="half" title="Pretty good"></label>
                                                    <input type="radio" id="star4" name="rating" value="4" <?php if($report_rating == '4') { echo 'checked="" class="check"'; }?> disabled=""><label for="star4" title="Pretty good"></label>
                                                    <input type="radio" id="star3.5" name="rating" value="3.5" <?php if($report_rating == '3.5') { echo 'checked="" class="check"'; }?> disabled=""><label for="star3.5" class="half" title="Meh"></label>
                                                    <input type="radio" id="star3" name="rating" value="3" <?php if($report_rating == '3') { echo 'checked="" class="check"'; }?> disabled=""><label for="star3" title="Meh"></label>
                                                    <input type="radio" id="star2.5" name="rating" value="2.5" <?php if($report_rating == '2.5') { echo 'checked="" class="check"'; }?> disabled=""><label for="star2.5" class="half" title="Kinda bad"></label>
                                                    <input type="radio" id="star2" name="rating" value="2" <?php if($report_rating == '2') { echo 'checked="" class="check"'; }?> disabled=""><label for="star2" title="Kinda bad"></label>
                                                    <input type="radio" id="star1.5" name="rating" value="1.5" <?php if($report_rating == '1.5') { echo 'checked="" class="check"'; }?> disabled=""><label for="star1.5" class="half" title="Sucks big time"></label>
                                                    <input type="radio" id="star1" name="rating" value="1" <?php if($report_rating == '1') { echo 'checked="" class="check"'; }?> disabled=""><label for="star1" title="Sucks big time"></label>
                                                </fieldset>
                                            <span class="reviews-count"><span itemprop="reviewCount" ><?php echo get_post_meta($post->ID, "report_rating", true); ?></span> Reviews</span>
                                        </div>
                               </div>
                            </a>
                    </div>
                    </main>
                </div>
    




<?php  } ?>