<?php
// Social Widget
class SocialCounter extends WP_Widget
{
    function SocialCounter(){
    $widget_ops = array('description' => 'A social widget where you can put your linkedin/facebook or youtube links');
    $control_ops = array('width' => 200, 'height' => 300);
    parent::__construct(false,$name='[Voxis] Social count widget',$widget_ops,$control_ops);
    }

  /* Displays the Widget in the front-end */
    function widget($args, $instance){
    extract($args);
    $linkedin = $instance['linkedin'];
    $facebook = $instance['facebook'];
    $youtube = $instance['youtube'];

    echo $before_widget;

    $nr = 0;
    if($linkedin != '')
        $nr++;
    if($facebook != '')
        $nr++;
    if($youtube != '')
        $nr++;
    $i = 0;
    echo '<div class="inner-sides">
        <div class="social-counter">';
    if($facebook != '') {
        $i++;
        echo '<div class="counter';
        if($i == 1)
            echo ' first-child';
        if($i == $nr)
            echo ' last-child';
        echo '">
            <a target="_blank" href="https://' . $facebook . '" class="facebook"><div class="inner-counter"><i class="fa fa-facebook"></i> ' . __('Like', 'Voxis') . '</div></a>
        </div>';

    }

    if($linkedin != '') {
        $i++;
        echo '<div class="counter';
        if($i == 1)
            echo ' first-child';
        if($i == $nr)
            echo ' last-child';
        echo '">
            <a target="_blank" href="https://' . $linkedin . '" class="linkedin"><div class="inner-counter"><i class="fa fa-linkedin"></i> ' . __('Connect', 'Voxis') . '</div></a>
        </div>';

    }

    if($youtube != '') {
        $i++;
        echo '<div class="counter';
        if($i == 1)
            echo ' first-child';
        if($i == $nr)
            echo ' last-child';
        echo '">
            <a target="_blank" href="https://' . $youtube . '" class="youtube"><div class="inner-counter"><i class="fa fa-youtube"></i> ' . __('Subscribe', 'Voxis') . '</div></a>
        </div>';
    }

    echo '</div>
    </div>';

    echo $after_widget;
  }

  /*Saves the settings. */
    function update($new_instance, $old_instance){
    $instance =  array();
    $instance['linkedin'] = esc_attr($new_instance['linkedin']);
    $instance['facebook'] = $new_instance['facebook'];
    $instance['youtube'] = $new_instance['youtube'];
    return $instance;
  }

  /*Creates the form for the widget in the back-end. */
    function form($instance){
    //Defaults
    $instance = wp_parse_args( (array) $instance, array('linkedin' => '', 'facebook'=> '', 'youtube' => '') );

    $linkedin = $instance['linkedin'];
    $facebook = $instance['facebook'];
    $youtube = $instance['youtube'];
    echo '<p><label for="' . $this->get_field_id('facebook') . '">' . 'Facebook url: ' . '</label><input id="' . $this->get_field_id('facebook') . '" name="' . $this->get_field_name('facebook') . '" value="'. esc_textarea($facebook)  . '" /></p>';
    echo '<p><label for="' . $this->get_field_id('linkedin') . '">' . 'linkedin url: ' . '</label><input id="' . $this->get_field_id('linkedin') . '" name="' . $this->get_field_name('linkedin') . '" value="'. esc_textarea($linkedin)  . '" /></p>';
    echo '<p><label for="' . $this->get_field_id('youtube') . '">' . 'youtube url: ' . '</label><input id="' . $this->get_field_id('youtube') . '" name="' . $this->get_field_name('youtube') . '" value="'. esc_textarea($youtube)  . '" /></p>';
  }

}// end SocialCounter class

// Tabbed Widget
class Tabbed extends WP_Widget
{
    function Tabbed(){
    $widget_ops = array('description' => 'Tabbed widget showing the most popular, recent and commented posts.');
    $control_ops = array('width' => 200, 'height' => 300);
    parent::__construct(false,$name='[Voxis] Tabbed widget',$widget_ops,$control_ops);
    }

  /* Displays the Widget in the front-end */
    function widget($args, $instance){
    extract($args);
    $number_posts = $instance['number_posts'];
    $id = rand(1, 50000);

    echo $before_widget; ?>

    <div class="tabbable"> 
        <ul class="nav nav-tabs">
            <li class="first-child active">
                <a data-toggle="tab" href="#tab1_<?php echo $id;?>"><div class="inner-tab"><?php _e('Popular', 'Voxis')?></div></a>
            </li>
            <li>
                <a data-toggle="tab" href="#tab2_<?php echo $id;?>"><div class="inner-tab"><?php _e('Recent', 'Voxis')?></div></a>
            </li>
            <li>
                <a data-toggle="tab" href="#tab3_<?php echo $id;?>"><div class="inner-tab"><?php _e('Random', 'Voxis')?></div></a>
            </li>
        </ul>
        <div class="tab-content">
            <div id="tab1_<?php echo $id;?>" class="tab-pane active">
                <ul>
                    <?php 
                    $args = array();
                    $args['posts_per_page'] = $number_posts;
                    $args['orderby'] = 'comment_count';
                    $args['order'] = 'DESC';
                    $query = new WP_Query($args);
                    while($query->have_posts() ) : $query->the_post(); global $post; ?>
                        <li>
                            <a title="<?php the_title();?>" href="<?php the_permalink();?>">
                                <figure>
                                    <?php if(has_post_thumbnail() ) { ?>
                                        <a href="<?php the_permalink();?>">
                                            <img src="<?php echo aq_resize(wp_get_attachment_url( get_post_thumbnail_id($post->ID) ), 42, 42, true); ?>" alt="<?php the_title();?>" />
                                        </a>
                                    <?php } else { ?>
                                        <a href="<?php the_permalink();?>">
                                            <img src="<?php echo get_template_directory_uri() . '/images/missing_56.png';?>" alt="<?php the_title();?>" />
                                        </a>
                                    <?php } ?>
                                </figure>
                                <p>
                                    <a href="<?php the_permalink();?>"><?php the_title();?></a> <br /> 
                                    <span><i class="fa fa-clock-o"></i> <?php the_time("F d, Y");?>, <i class="fa fa-comments-o"></i> <?php comments_popup_link(esc_html__('0 comments','Voxis'), esc_html__('1 comment','Voxis'), '% '.esc_html__('comments','Voxis')); ?> </span>
                                </p>
                            </a>
                        </li>
                    <?php endwhile; wp_reset_postdata(); ?>
                </ul>
            </div>
            <div id="tab2_<?php echo $id;?>" class="tab-pane">
                <ul>
                    <?php 
                    $args = array();
                    $args['posts_per_page'] = $number_posts;
                    $query = new WP_Query($args);
                    while($query->have_posts() ) : $query->the_post(); global $post ?>
                        <li>
                            <a title="<?php the_title();?>" href="<?php the_permalink();?>">
                                <figure>
                                    <?php if(has_post_thumbnail() ) { ?>
                                        <a href="<?php the_permalink();?>">
                                            <img src="<?php echo aq_resize(wp_get_attachment_url( get_post_thumbnail_id($post->ID) ), 42, 42, true); ?>" alt="<?php the_title();?>" />
                                        </a>
                                    <?php } else { ?>
                                        <a href="<?php the_permalink();?>">
                                            <img src="<?php echo get_template_directory_uri() . '/images/missing_56.png';?>" alt="<?php the_title();?>" />
                                        </a>
                                    <?php } ?>
                                </figure>
                                <p>
                                    <a href="<?php the_permalink();?>"><?php the_title();?></a> <br /> 
                                    <span><i class="fa fa-clock-o"></i> <?php the_time("F d, Y");?>, <i class="fa fa-comments-o"></i> <?php comments_popup_link(esc_html__('0 comments','Voxis'), esc_html__('1 comment','Voxis'), '% '.esc_html__('comments','Voxis')); ?> </span>
                                </p>
                            </a>
                        </li>
                    <?php endwhile; wp_reset_postdata(); ?>

                </ul>
            </div>
            <div id="tab3_<?php echo $id;?>" class="tab-pane">
                <ul>
                    <?php 
                    $args = array();
                    $args['posts_per_page'] = $number_posts;
                    $args['orderby'] = 'rand';
                    $query = new WP_Query($args);
                    while($query->have_posts() ) : $query->the_post(); global $post; ?>
                        <li>
                            <a title="<?php the_title();?>" href="<?php the_permalink();?>">
                                <figure>
                                    <?php if(has_post_thumbnail() ) { ?>
                                        <a href="<?php the_permalink();?>">
                                            <img src="<?php echo aq_resize(wp_get_attachment_url( get_post_thumbnail_id($post->ID) ), 42, 42, true); ?>" alt="<?php the_title();?>" />
                                        </a>
                                    <?php } else { ?>
                                        <a href="<?php the_permalink();?>">
                                            <img src="<?php echo get_template_directory_uri() . '/images/missing_56.png';?>" alt="<?php the_title();?>" />
                                        </a>
                                    <?php } ?>
                                </figure>
                                <p>
                                    <a href="<?php the_permalink();?>"><?php the_title();?></a> <br /> 
                                    <span><i class="fa fa-clock-o"></i> <?php the_time("F d, Y");?>, <i class="fa fa-comments-o"></i> <?php comments_popup_link(esc_html__('0 comments','Voxis'), esc_html__('1 comment','Voxis'), '% '.esc_html__('comments','Voxis')); ?></span>
                                </p>
                            </a>
                        </li>
                    <?php endwhile; wp_reset_postdata(); ?>

                </ul>
            </div>
        </div>
    </div>

    <?php
    echo $after_widget;
  }

  /*Saves the settings. */
    function update($new_instance, $old_instance){
    $instance =  array();
    $instance['number_posts'] = (int)$new_instance['number_posts'];

    return $instance;
  }

  /*Creates the form for the widget in the back-end. */
    function form($instance){
    //Defaults
    $instance = wp_parse_args( (array) $instance, array('number_posts'=> 5) );

    $number_posts = (int) $instance['number_posts'];
    echo '<p><label for="' . $this->get_field_id('number_posts') . '">' . 'Number of posts: ' . '</label><input id="' . $this->get_field_id('number_posts') . '" name="' . $this->get_field_name('number_posts') . '" value="'. esc_textarea($number_posts)  . '" /></p>';
  }

}// end Tabbed class

// NewsInPictures Widget
class NewsInPictures extends WP_Widget
{
    function NewsInPictures(){
    $widget_ops = array('description' => 'Shows your latest posts presented with their images.');
    $control_ops = array('width' => 200, 'height' => 300);
    parent::__construct(false,$name='[Voxis] News in pictures',$widget_ops,$control_ops);
    }

  /* Displays the Widget in the front-end */
    function widget($args, $instance){
    extract($args);

    $title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title']);
    $number_posts = $instance['number_posts'];
    $categories = $instance['categories'];

    echo $before_widget; 

    if ( $title != '' )
        echo $before_title . $title . $after_title;

    echo '
    <div class="photo-list">';
    $args = array();
    if(!empty($categories) )
        $args['category__in'] = $categories;
    $args['posts_per_page'] = $number_posts != 0 ? $number_posts : 20;
    $query = new WP_Query($args);
    while($query->have_posts() ) : $query->the_post(); global $post; ?>
        <div class='photo'>
            <?php 
            if(has_post_thumbnail() ) 
                $thumb = aq_resize(wp_get_attachment_url( get_post_thumbnail_id($post->ID) ), 57, 57, true);
            else
                $thumb = get_template_directory_uri() . '/img/missing_56.png';
            ?>
            <a href="<?php the_permalink();?>" data-toggle="tooltip" title="<?php the_title();?>">
                <img src="<?php echo $thumb; ?>" alt="<?php the_title();?>" />
            </a>
        </div>
    <?php 
    endwhile; wp_reset_postdata(); 
    echo '</div>';

    echo $after_widget;
  }

  /*Saves the settings. */
    function update($new_instance, $old_instance){
    $instance =  array();
    $instance['title'] = esc_attr($new_instance['title']);
    $instance['number_posts'] = (int)$new_instance['number_posts'];
    $instance['categories'] = $new_instance['categories'];

    return $instance;
  }

  /*Creates the form for the widget in the back-end. */
    function form($instance){
    //Defaults
    $instance = wp_parse_args( (array) $instance, array('number_posts'=> 5, 'title' => '', 'categories' => array() ) );

    $title = esc_attr($instance['title']);
    $number_posts = (int) $instance['number_posts'];
    $categories = (array) $instance['categories'];
    
    echo '<p><label for="' . $this->get_field_id('title') . '">' . 'Title: ' . '</label><input id="' . $this->get_field_id('title') . '" name="' . $this->get_field_name('title') . '" value="'. esc_textarea($title)  . '" /></p>';
    echo '<p><label for="' . $this->get_field_id('number_posts') . '">' . 'Number of posts: ' . '</label><input id="' . $this->get_field_id('number_posts') . '" name="' . $this->get_field_name('number_posts') . '" value="'. esc_textarea($number_posts)  . '" /></p>';
    $cats = get_categories('hide_empty=0');
    ?>
    <p>
        <label>Categories to include: </label> <br />
        <?php foreach( $cats as $category ) { ?>
            <label>
                <input type="checkbox" name="<?php echo $this->get_field_name('categories'); ?>[]" value="<?php echo $category->cat_ID; ?>"  <?php if(in_array( $category->cat_ID, $categories ) ) echo 'checked="checked" '; ?> /> 
                <?php echo $category->cat_name; ?>
            </label> <br />
        <?php } ?>
    </p> 
    <?php
  }

}// end NewsInPictures class

// PopularPosts Widget
class PopularPosts extends WP_Widget
{
    function PopularPosts(){
    $widget_ops = array('description' => 'Shows the most commented posts.');
    $control_ops = array('width' => 200, 'height' => 300);
    parent::__construct(false,$name='[Voxis] Popular posts',$widget_ops,$control_ops);
    }

  /* Displays the Widget in the front-end */
    function widget($args, $instance){
    extract($args);

    $title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title']);
    $number_posts = $instance['number_posts'];
    $categories = $instance['categories'];

    echo $before_widget; 

    if ( $title != '' )
        echo $before_title . $title . $after_title;

    echo '
    <div class="list">
        <ul>';
    $args = array();
    $args['posts_per_page'] = $number_posts;
    $args['orderby'] = 'comment_count';
    $args['order'] = 'DESC';
    if(!empty($categories) )
        $args['category__in'] = $categories;
    $query = new WP_Query($args);
    while($query->have_posts() ) : $query->the_post(); global $post; ?>
        <li>
            <a title="<?php the_title();?>" href="<?php the_permalink();?>">
                <figure>
                    <?php if(has_post_thumbnail() ) { ?>
                        <a href="<?php the_permalink();?>">
                            <img src="<?php echo aq_resize(wp_get_attachment_url( get_post_thumbnail_id($post->ID) ), 42, 42, true); ?>" alt="<?php the_title();?>" />
                        </a>
                    <?php } else { ?>
                        <a href="<?php the_permalink();?>">
                            <img src="<?php echo get_template_directory_uri() . '/img/missing_56.png';?>" alt="<?php the_title();?>" />
                        </a>
                    <?php } ?>
                </figure>
                <p>
                    <a href="<?php the_permalink();?>"><?php the_title();?></a> <br /> 
                    <span><i class="fa fa-clock-o"></i> <?php the_time("F d, Y");?>, <i class="fa fa-comments-o"></i> <?php comments_popup_link(esc_html__('0 comments','Voxis'), esc_html__('1 comment','Voxis'), '% '.esc_html__('comments','Voxis')); ?> </span>
                </p>
            </a>
        </li>
    <?php endwhile; wp_reset_postdata();
    echo '</ul>
    </div>';

    echo $after_widget;
  }

  /*Saves the settings. */
    function update($new_instance, $old_instance){
    $instance =  array();
    $instance['title'] = esc_attr($new_instance['title']);
    $instance['number_posts'] = (int)$new_instance['number_posts'];
    $instance['categories'] = $new_instance['categories'];

    return $instance;
  }

  /*Creates the form for the widget in the back-end. */
    function form($instance){
    //Defaults
    $instance = wp_parse_args( (array) $instance, array('number_posts'=> 5, 'title' => '', 'categories' => array() ) );

    $title = esc_attr($instance['title']);
    $number_posts = (int) $instance['number_posts'];
    $categories = (array) $instance['categories'];
    
    echo '<p><label for="' . $this->get_field_id('title') . '">' . 'Title: ' . '</label><input id="' . $this->get_field_id('title') . '" name="' . $this->get_field_name('title') . '" value="'. esc_textarea($title)  . '" /></p>';
    echo '<p><label for="' . $this->get_field_id('number_posts') . '">' . 'Number of posts: ' . '</label><input id="' . $this->get_field_id('number_posts') . '" name="' . $this->get_field_name('number_posts') . '" value="'. esc_textarea($number_posts)  . '" /></p>';
    $cats = get_categories('hide_empty=0');
    ?>
    <p>
        <label>Categories to include: </label> <br />
        <?php foreach( $cats as $category ) { ?>
            <label>
                <input type="checkbox" name="<?php echo $this->get_field_name('categories'); ?>[]" value="<?php echo $category->cat_ID; ?>"  <?php if(in_array( $category->cat_ID, $categories ) ) echo 'checked="checked" '; ?> /> 
                <?php echo $category->cat_name; ?>
            </label> <br />
        <?php } ?>
    </p> 
    <?php
  }

}// end PopularPosts class

// RandomPosts Widget
class RandomPosts extends WP_Widget
{
    function RandomPosts(){
    $widget_ops = array('description' => 'Shows some random posts from your website.');
    $control_ops = array('width' => 200, 'height' => 300);
    parent::__construct(false,$name='[Voxis] Random posts',$widget_ops,$control_ops);
    }

  /* Displays the Widget in the front-end */
    function widget($args, $instance){
    extract($args);

    $title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title']);
    $number_posts = $instance['number_posts'];

    echo $before_widget; 

    if ( $title != '' )
        echo $before_title . $title . $after_title;

    echo '
    <div class="list">
        <ul>';
    $args = array();
    $args['posts_per_page'] = $number_posts;
    $args['orderby'] = 'rand';
    $query = new WP_Query($args);
    while($query->have_posts() ) : $query->the_post(); global $post; ?>
        <li>
            <a title="<?php the_title();?>" href="<?php the_permalink();?>">
                <figure>
                    <?php if(has_post_thumbnail() ) { ?>
                        <a href="<?php the_permalink();?>">
                            <img src="<?php echo aq_resize(wp_get_attachment_url( get_post_thumbnail_id($post->ID) ), 42, 42, true); ?>" alt="<?php the_title();?>" />
                        </a>
                    <?php } else { ?>
                        <a href="<?php the_permalink();?>">
                            <img src="<?php echo get_template_directory_uri() . '/img/missing_56.png';?>" alt="<?php the_title();?>" />
                        </a>
                    <?php } ?>
                </figure>
                <p>
                    <a href="<?php the_permalink();?>"><?php the_title();?></a> <br /> 
                    <span><i class="fa fa-clock-o"></i> <?php the_time("F d, Y");?>, <i class="fa fa-comments-o"></i> <?php comments_popup_link(esc_html__('0 comments','Voxis'), esc_html__('1 comment','Voxis'), '% '.esc_html__('comments','Voxis')); ?> </span>
                </p>
            </a>
        </li>
    <?php endwhile; wp_reset_postdata();
    echo '</ul>
    </div>';

    echo $after_widget;
  }

  /*Saves the settings. */
    function update($new_instance, $old_instance){
    $instance =  array();
    $instance['title'] = esc_attr($new_instance['title']);
    $instance['number_posts'] = (int)$new_instance['number_posts'];

    return $instance;
  }

  /*Creates the form for the widget in the back-end. */
    function form($instance){
    //Defaults
    $instance = wp_parse_args( (array) $instance, array('number_posts'=> 5, 'title' => '' ) );

    $title = esc_attr($instance['title']);
    $number_posts = (int) $instance['number_posts'];
    
    echo '<p><label for="' . $this->get_field_id('title') . '">' . 'Title: ' . '</label><input id="' . $this->get_field_id('title') . '" name="' . $this->get_field_name('title') . '" value="'. esc_textarea($title)  . '" /></p>';
    echo '<p><label for="' . $this->get_field_id('number_posts') . '">' . 'Number of posts: ' . '</label><input id="' . $this->get_field_id('number_posts') . '" name="' . $this->get_field_name('number_posts') . '" value="'. esc_textarea($number_posts)  . '" /></p>';
  }

}// end RandomPosts class

// RecentPosts Widget
class RecentPosts extends WP_Widget
{
    function RecentPosts(){
    $widget_ops = array('description' => 'Shows the most recent posts from the specified categories.');
    $control_ops = array('width' => 200, 'height' => 300);
    parent::__construct(false,$name='[Voxis] Recent posts',$widget_ops,$control_ops);
    }

  /* Displays the Widget in the front-end */
    function widget($args, $instance){
    extract($args);

    $title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title']);
    $number_posts = $instance['number_posts'];
    $categories = $instance['categories'];

    echo $before_widget; 

    if ( $title != '' )
        echo $before_title . $title . $after_title;

    echo '
    <div class="list">
        <ul>';
    $args = array();
    $args['posts_per_page'] = $number_posts;
    if(!empty($categories) )
        $args['category__in'] = $categories;
    $query = new WP_Query($args);
    while($query->have_posts() ) : $query->the_post(); global $post; ?>
        <li>
            <a title="<?php the_title();?>" href="<?php the_permalink();?>">
                <figure>
                    <?php if(has_post_thumbnail() ) { ?>
                        <a href="<?php the_permalink();?>">
                            <img src="<?php echo aq_resize(wp_get_attachment_url( get_post_thumbnail_id($post->ID) ), 42, 42, true); ?>" alt="<?php the_title();?>" />
                        </a>
                    <?php } else { ?>
                        <a href="<?php the_permalink();?>">
                            <img src="<?php echo get_template_directory_uri() . '/img/missing_56.png';?>" alt="<?php the_title();?>" />
                        </a>
                    <?php } ?>
                </figure>
                <p>
                    <a href="<?php the_permalink();?>"><?php the_title();?></a> <br /> 
                    <span><i class="fa fa-clock-o"></i> <?php the_time("F d, Y");?>, <i class="fa fa-comments-o"></i> <?php comments_popup_link(esc_html__('0 comments','Voxis'), esc_html__('1 comment','Voxis'), '% '.esc_html__('comments','Voxis')); ?> </span>
                </p>
            </a>
        </li>
    <?php endwhile; wp_reset_postdata();
    echo '</ul>
    </div>';

    echo $after_widget;
  }

  /*Saves the settings. */
    function update($new_instance, $old_instance){
    $instance =  array();
    $instance['title'] = esc_attr($new_instance['title']);
    $instance['number_posts'] = (int)$new_instance['number_posts'];
    $instance['categories'] = $new_instance['categories'];

    return $instance;
  }

  /*Creates the form for the widget in the back-end. */
    function form($instance){
    //Defaults
    $instance = wp_parse_args( (array) $instance, array('number_posts'=> 5, 'title' => '', 'categories' => array() ) );

    $title = esc_attr($instance['title']);
    $number_posts = (int) $instance['number_posts'];
    $categories = (array) $instance['categories'];
    
    echo '<p><label for="' . $this->get_field_id('title') . '">' . 'Title: ' . '</label><input id="' . $this->get_field_id('title') . '" name="' . $this->get_field_name('title') . '" value="'. esc_textarea($title)  . '" /></p>';
    echo '<p><label for="' . $this->get_field_id('number_posts') . '">' . 'Number of posts: ' . '</label><input id="' . $this->get_field_id('number_posts') . '" name="' . $this->get_field_name('number_posts') . '" value="'. esc_textarea($number_posts)  . '" /></p>';
    $cats = get_categories('hide_empty=0');
    ?>
    <p>
        <label>Categories to include: </label> <br />
        <?php foreach( $cats as $category ) { ?>
            <label>
                <input type="checkbox" name="<?php echo $this->get_field_name('categories'); ?>[]" value="<?php echo $category->cat_ID; ?>"  <?php if(in_array( $category->cat_ID, $categories ) ) echo 'checked="checked" '; ?> /> 
                <?php echo $category->cat_name; ?>
            </label> <br />
        <?php } ?>
    </p> 
    <?php
  }

}// end RecentPosts class

// RecentComments Widget
class RecentComments extends WP_Widget
{
    function RecentComments(){
    $widget_ops = array('description' => 'Shows the most recent comments.');
    $control_ops = array('width' => 200, 'height' => 300);
    parent::__construct(false,$name='[Voxis] Recent comments',$widget_ops,$control_ops);
    }

  /* Displays the Widget in the front-end */
    function widget($args, $instance){
    extract($args);

    $title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title']);
    $number_posts = $instance['number_posts'] != 0 ? (int)$instance['number_posts'] : 5;

    echo $before_widget; 

    if ( $title != '' )
        echo $before_title . $title . $after_title;

    echo '
    <div class="list">
        <ul>';
    
    $comments = get_comments('status=approve&number=' . $number_posts); 
    foreach($comments as $comment) { ?>
        <li>
            <figure>
                <a href="<?php echo get_comment_link($comment->comment_ID);?>">
                    <?php echo get_avatar($comment->comment_author_email, 42);?>
                </a>
            </figure>
            <p>
                <a href="<?php echo get_comment_link($comment->comment_ID);?>"><?php echo $comment->comment_author . ' - ' . truncate_text($comment->comment_content, 80, false);?></a> <br /> 
                <span> <?php _e('on ', 'Voxis'); ?> <a href="<?php echo get_permalink($comment->comment_post_ID);?>"><?php echo get_the_title($comment->comment_post_ID);?></a></span>
            </p>
        </li>
    <?php } 
    echo '</ul>
    </div>';

    echo $after_widget;
  }

  /*Saves the settings. */
    function update($new_instance, $old_instance){
    $instance =  array();
    $instance['title'] = esc_attr($new_instance['title']);
    $instance['number_posts'] = (int)$new_instance['number_posts'];

    return $instance;
  }

  /*Creates the form for the widget in the back-end. */
    function form($instance){
    //Defaults
    $instance = wp_parse_args( (array) $instance, array('number_posts'=> 5, 'title' => '' ) );

    $title = esc_attr($instance['title']);
    $number_posts = (int) $instance['number_posts'];
    
    echo '<p><label for="' . $this->get_field_id('title') . '">' . 'Title: ' . '</label><input id="' . $this->get_field_id('title') . '" name="' . $this->get_field_name('title') . '" value="'. esc_textarea($title)  . '" /></p>';
    echo '<p><label for="' . $this->get_field_id('number_posts') . '">' . 'Number of comments: ' . '</label><input id="' . $this->get_field_id('number_posts') . '" name="' . $this->get_field_name('number_posts') . '" value="'. esc_textarea($number_posts)  . '" /></p>';
  }

}// end RecentComments class

// FeaturedVideo Widget
class FeaturedVideo extends WP_Widget
{
    function FeaturedVideo(){
    $widget_ops = array('description' => 'Shows a video in the sidebar.');
    $control_ops = array('width' => 200, 'height' => 300);
    parent::__construct(false,$name='[Voxis] Featured Video',$widget_ops,$control_ops);
    }

  /* Displays the Widget in the front-end */
    function widget($args, $instance){
    extract($args);

    $title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title']);
    $embed_code = $instance['embed_code'];

    echo $before_widget; 

    if ( $title != '' )
        echo $before_title . $title . $after_title;

    echo '<div class="featured-video">' . $embed_code . '</div>';

    echo $after_widget;
  }

  /*Saves the settings. */
    function update($new_instance, $old_instance){
    $instance =  array();
    $instance['title'] = esc_attr($new_instance['title']);
    $instance['embed_code'] = $new_instance['embed_code'];

    return $instance;
  }

  /*Creates the form for the widget in the back-end. */
    function form($instance){
    //Defaults
    $instance = wp_parse_args( (array) $instance, array('embed_code'=> '', 'title' => '' ) );

    $title = esc_attr($instance['title']);
    $embed_code = $instance['embed_code'];
    
    echo '<p><label for="' . $this->get_field_id('title') . '">' . 'Title: ' . '</label><input id="' . $this->get_field_id('title') . '" name="' . $this->get_field_name('title') . '" value="'. esc_textarea($title)  . '" /></p>';
    echo '<p><label for="' . $this->get_field_id('embed_code') . '">' . 'Embed code(iframe tag): ' . '</label><input class="widefat" id="' . $this->get_field_id('embed_code') . '" name="' . $this->get_field_name('embed_code') . '" value="'. esc_textarea($embed_code)  . '" /></p>';
  }

}// end FeaturedVideo class

function VoxisWidgets() {
  register_widget('SocialCounter');
  register_widget('Tabbed');
  register_widget('NewsInPictures');
  register_widget('PopularPosts');
  register_widget('RandomPosts');
  register_widget('RecentPosts');
  register_widget('RecentComments');
  register_widget('FeaturedVideo');
}

add_action('widgets_init', 'VoxisWidgets');