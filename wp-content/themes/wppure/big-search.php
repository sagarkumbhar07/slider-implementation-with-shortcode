<section class="search-section">
    <div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 text-center">
				<div class="blog-title">
                    <div class="header-search-wrap">
                        <form class="form form-horizontal validate-form" id="header-search-form" method="get" role="search" enctype="multipart/form-data" novalidate="novalidate" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for Reports here ..." placeholder="<?php esc_attr_e( 'Search here &hellip;', 'ta-meghna' ); ?>" autocomplete="on" name="s">  
                            <span class="input-group-btn">
                            <button class="btn btn-search" type="submit" title="<?php esc_attr_e( 'Search', 'ta-meghna' ); ?>" id="search-submit-button"><i class="fa fa-search"></i> Search</button>
                            </span>     
                        </div>
                    </form>     
                    </div>				
                </div>
			</div>
		</div>
	</div>
</section>