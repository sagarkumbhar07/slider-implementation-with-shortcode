(function($){
  /*
  * --------------------------------------------------------------------------------------------------------------------
  * form validation
  * --------------------------------------------------------------------------------------------------------------------
  */

  $('[data-toggle="tooltip"]').tooltip(); 

   jQuery.setValidateForm = function(selector) {
     // alert('start');
    if (selector == null) {
      selector = $(".validate-form");
    }
    if (jQuery().validate) {
      return selector.each(function(i, elem) {
        return $(elem).validate({
          errorElement: "span",
          errorClass: "help-block has-error",
          errorPlacement: function(e, t) {
            return t.parents(".controls").first().append(e);
          },
          highlight: function(e) {
            return $(e).closest('.form-group').removeClass("has-error has-success").addClass('has-error');
          },
          success: function(e) {
            return e.closest(".form-group").removeClass("has-error");
          }
        });
      });
    }
  };
}(jQuery));


 jQuery.setValidateForm();
(function($) {

    jQuery.validator.addMethod("alphanumericspecial", function(value, element) {
    return this.optional(element) || value == value.match(/^[-a-zA-Z0-9_ ]+$/);
    }, "Only letters, Numbers & Space/underscore Allowed.");

    jQuery.validator.addMethod("alpha", function(value, element) {
    return this.optional(element) || value == value.match(/^[a-zA-Z]+$/);
    },"Only Characters Allowed.");

    jQuery.validator.addMethod("alphanumeric", function(value, element) {
    return this.optional(element) || value == value.match(/^[a-z0-9A-Z#]+$/);
    },"Only Characters, Numbers & Hash Allowed.");

	"use strict";

    /*
	  =======================================================================
		  		LightGallery
	  =======================================================================
	*/
    jQuery(".image-popup").lightGallery({
        selector: "this",
        cssEasing: "cubic-bezier(0.25, 0, 0.25, 1)",
        download: false,
        counter: false
    });
    var jQuerylg = jQuery(".lightgallery"), dlt = jQuerylg.data("looped");
    jQuerylg.lightGallery({
        selector: ".lightgallery a.popup-image , .lightgallery  a.popgal",
        cssEasing: "cubic-bezier(0.25, 0, 0.25, 1)",
        download: false,
        loop: false
    });



	/* ========================================================================= */
	/*	Scroll Up / Back to top
	/* ========================================================================= */
	$(window).scroll(function() {
		if ($(window).scrollTop() > 400) {
			$("#scrollUp").fadeIn(200);
		} else {
			$("#scrollUp").fadeOut(200);
		}
	});

	$('#scrollUp').click(function() {
        //alert('start');
		 $('html,body').animate({ scrollTop: 0 }, 'slow');
        return false;
	});

	/* ========================================================================= */
	/*	Post image slider
	/* ========================================================================= */
	$("#post-thumb, #gallery-post").owlCarousel({
		navigation : true,
		pagination : false,
		slideSpeed : 800,
		autoHeight : true,
		paginationSpeed : 800,
		singleItem:true,
		navigationText : ["<i class='fa fa-angle-left fa-2x'></i>","<i class='fa fa-angle-right fa-2x'></i>"]

	});

	$("#main-features").owlCarousel({
		navigation : false,
		pagination : true,
		slideSpeed : 800,
		singleItem : true,
		transitionStyle : "fadeUp",
		paginationSpeed : 800,
	});

	/* ========================================================================= */
	/*	Menu item highlighting
	/* ========================================================================= */
	$("#navigation").sticky({
		topSpacing : 0
	});

	/* ========================================================================= */
	/*	Fix Slider Height
	/* ========================================================================= */	

	var slideHeight = $(window).height();

	$('#slider, .sl-slider, .sl-content-wrapper').css('height',slideHeight);

	$(window).resize(function(){
		'use strict',
		$('#slider, .sl-slider, .sl-content-wrapper').css('height',slideHeight);
	});

	/* ========================================================================= */
	/*	Portfolio Filtering Hook
	/* =========================================================================  */
	//$('#og-grid').mixItUp(); // Portfolio filter
	
	//Grid.init(); //Portfolio Grid Expand

	/* ========================================================================= */
	/*	Testimonial Carousel
	/* =========================================================================  */
	//Init the carousel
	$("#testimonial-block").owlCarousel({
		slideSpeed: 500,
		paginationSpeed: 500,
		singleItem: true,
		pagination : true,
		transitionStyle : "backSlide"
	});

	/* ========================================================================= */
	/*   Contact Form Validating
	/* ========================================================================= */
	$('#contact-submit').click(function (e) {
		//stop the form from being submitted
		e.preventDefault();

		/* declare the variables, var error is the variable that we use on the end
		to determine if there was an error or not */
		var error = false;
		var name = $('#name').val();
		var email = $('#email').val();
		var subject = $('#subject').val();
		var message = $('#message').val();

		/* in the next section we do the checking by using VARIABLE.length
		where VARIABLE is the variable we are checking (like name, email),
		length is a JavaScript function to get the number of characters.
		And as you can see if the num of characters is 0 we set the error
		variable to true and show the name_error div with the fadeIn effect. 
		if it's not 0 then we fadeOut the div( that's if the div is shown and
		the error is fixed it fadesOut. 

		The only difference from these checks is the email checking, we have
		email.indexOf('@') which checks if there is @ in the email input field.
		This JavaScript function will return -1 if no occurrence have been found.*/
		if (name.length == 0) {
			var error = true;
			$('#name').css("border-color", "#D8000C");
		} else {
			$('#name').css("border-color", "#666");
		}
		if (email.length == 0 || email.indexOf('@') == '-1') {
			var error = true;
			$('#email').css("border-color", "#D8000C");
		} else {
			$('#email').css("border-color", "#666");
		}
		if (subject.length == 0) {
			var error = true;
			$('#subject').css("border-color", "#D8000C");
		} else {
			$('#subject').css("border-color", "#666");
		}
		if (message.length == 0) {
			var error = true;
			$('#message').css("border-color", "#D8000C");
		} else {
			$('#message').css("border-color", "#666");
		}

		//now when the validation is done we check if the error variable is false (no errors)
		if (error == false) {
			//disable the submit button to avoid spamming
			//and change the button text to Sending...
			$('#contact-submit').attr({
				'disabled': 'false',
				'value': 'Sending...'
			});

			/* using the jquery's post(ajax) function and a lifesaver
			function serialize() which gets all the data from the form
			we submit it to send_email.php */
			$.post(""+ta_script_vars.templateUrl+"/inc/sendmail.php", $("#contact-form").serialize(), function (result) {
				//and after the ajax request ends we check the text returned
				if (result == 'sent') {
					//if the mail is sent remove the submit paragraph
					$('#cf-submit').remove();
					//and show the mail success div with fadeIn
					$('#mail-success').fadeIn(500);
				} else {
					//show the mail failed div
					$('#mail-fail').fadeIn(500);
					//re enable the submit button by removing attribute disabled and change the text back to Send The Message
					$('#contact-submit').removeAttr('disabled').attr('value', 'Send The Message');
				}
			});
		}
	});

}(jQuery));

/* ========================================================================= */
/*	On scroll fade/bounce effect
/* ========================================================================= */
wow = new WOW({
	animateClass: 'animated',
	offset: 120
});
wow.init();


/* ========================================================================= */
/*	Add specific styles
/* ========================================================================= */
(function($) {
	$(".about-section #about-item:last-child").find(".wrap-about").addClass("kill-margin-bottom");
	$(".counter-section #count-item:last-child").find(".counters-item").addClass("kill-margin-bottom");
	$(".services-section #service-item:last-child").find(".service-block").addClass("kill-margin-bottom");
	$(".our-team #team-member:last-child").find(".team-mate").addClass("kill-margin-bottom");
	$(".pricing-section #single-pricing:last-child").find(".pricing").addClass("kill-margin-bottom");
	$(".blog-section #single-blog:last-child").find(".note").addClass("kill-margin-bottom");
	$("table#wp-calendar").addClass("table table-striped");
	$("select").addClass("form-control");
	$(".comment-reply-link").addClass("reply pull-right");
	$(".widget_rss ul").addClass("media-list");
}(jQuery));