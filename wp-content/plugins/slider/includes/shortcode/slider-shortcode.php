<?php 
add_shortcode('wpsagar', 'wp_sagar_slider');

function slider_box($atts)
{

?>
<div class="row">
	<div class="col-lg-10 no-mar-pad">
<div class="owl-carousel top owl-theme">
	<?php 
$active = 'active';
$args = array();
$args['posts_per_page'] = -1;
$args['orderby'] = 'date';
$args['order'] = 'DESC';
$args['post_type'] = 'slider';
$args['order'] = 'DESC';
$query = new WP_Query($args);

$i = 1;

while($query->have_posts() ) : $query->the_post(); global $post;
	?>
<div class="item review-section-box">
<div class="review-section-wrap-content">
<?php if ( has_post_thumbnail() ) { ?>
	<?php the_post_thumbnail( 'full', array('class' => "img-responsive" ) ); ?>
<?php } ?>
<div class="slider-content">
<h3><?php echo $slider_title = get_post_meta($post->ID, "slider_title", true);?></h3>
<p><?php echo $slider_desc = get_post_meta($post->ID, "slider_desc", true); ?></p>
</div>
</div>
</div>
<?php 
	$i++;
	endwhile; 
	wp_reset_postdata(); 
?>
</div>
</div>
<div class="col-lg-2 no-mar-pad">
	<div class="slider-background">
		<i class="fa fa-bars" aria-hidden="true"></i>
	</div>
</div>

</div>
<script type="text/javascript">
	jQuery(document).ready(function($){jQuery('.top').owlCarousel({scrollPerPage: true,items:1,
	itemsDesktop: [1199,1],itemsDesktopSmall: [979,2],itemsTablet:[768,1],itemsTabletSmall: [640,1],
	itemsMobile: [479,1],autoPlay:true,stopOnHover: true,pagination: true,rewindSpeed: 1000,slideSpeed:200,
	paginationSpeed: "800"})});
</script>
<?php } add_shortcode('sagarwpslider', 'slider_box'); ?>