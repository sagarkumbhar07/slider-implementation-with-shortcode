<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://wppurecss.com
 * @since      1.0.0
 *
 * @package    Slider
 * @subpackage Slider/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Slider
 * @subpackage Slider/includes
 * @author     Sagar Kumbhar <kumbharsagar23694@gmail.com>
 */
class Slider_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
