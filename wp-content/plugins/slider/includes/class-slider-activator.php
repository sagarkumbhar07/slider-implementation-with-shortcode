<?php

/**
 * Fired during plugin activation
 *
 * @link       https://wppurecss.com
 * @since      1.0.0
 *
 * @package    Slider
 * @subpackage Slider/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Slider
 * @subpackage Slider/includes
 * @author     Sagar Kumbhar <kumbharsagar23694@gmail.com>
 */
class Slider_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
