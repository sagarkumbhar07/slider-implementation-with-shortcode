<?php
//require_once 'MBiz_REST_Posts_Controller.php';

add_theme_support('post-thumbnails', array('post', 'page'));
//flush_rewrite_rules();
add_theme_support('post-thumbnails');
set_post_thumbnail_size(200, 200, true); 
add_image_size('single-post-thumbnail', 9999, 150, false);
// *********************************** WP Admin Customer Custom Post Type Start *************************************//
add_action('init', 'create_slider');
function create_slider()
{
    register_post_type('slider',
        array(
            'labels' => array(
                'name' => 'Slider',
                'singular_name' => 'Slider',
                'add_new' => 'Add Slider Data',
                'add_new_item' => ' ',
                'edit' => 'Edit',
                'edit_item' => 'Edit Slider Data',
                'new_item' => 'New Slider Data',
                'view' => 'View',
                'view_item' => 'View Slider Data',
                'search_items' => 'Search Slider Data',
                'not_found' => 'No Slider Data found',
                'not_found_in_trash' => 'No Slider Data found in Trash',
                'parent' => 'Parent Slider Data'
            ),

            'public' => false,  // it's not public, it shouldn't have it's own permalink, and so on
            'publicly_queriable' => true,  // you should be able to query it
            'show_ui' => true,  // you should be able to edit it in wp-admin
            'exclude_from_search' => false,  // you should exclude it from search results
            'show_in_nav_menus' => false,  // you shouldn't be able to add it to menus
            'has_archive' => false,  // it shouldn't have archive page
            'rewrite' => false,  // it shouldn't have rewrite rules
            'exclude_from_search' => true,
            'menu_position' => 15,
            'supports' => array( 'title', 'thumbnail' ),
            'taxonomies' => array(''),
            //'menu_icon' => plugins_url('images/customer.png', __FILE__),
            'menu_icon' => 'dashicons-format-gallery',
            'has_archive' => true,
            'register_meta_box_cb' => 'display_meta_slider',

        'show_in_rest'       => true,
        'rest_base'          => 'sliders',
        'rest_controller_class' => 'MBiz_REST_Posts_Controller',
        )
    );
    flush_rewrite_rules();
}
// *********************************** WP Admin Customer Custom Post Type End *************************************//


add_action( 'rest_api_init', 'slug_register_starship3' );
function slug_register_starship3() {
    register_rest_field( 'customer',
        'meta',
        array(
            'get_callback'    => 'slug_get_starship3',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

/**
 * Get the value of the "starship" field
 *
 * @param array $object Details of current post.
 * @param string $field_name Name of field.
 * @param WP_REST_Request $request Current request
 *
 * @return mixed
 */
function slug_get_starship3( $object, $field_name, $request ) {
        $meta = get_post_meta( $object[ 'id' ], '', true );
        return $meta;
}



// *********************************** WP Admin Customer Custom Columns Display Start *************************************//

    //Adding Custom Columns start
    add_filter('manage_slider_posts_columns', 'slider_columns_add');
    function slider_columns_add($columns) {
        // $columns['image'] = 'Photo';
        $columns['slider_id'] = 'ID';
        $columns['slider_title'] = 'Slider Title';
        $columns['slider_desc'] = 'Slider Description';
        return $columns;
    }

    //Manage Custom Columns start
    add_action('manage_slider_posts_custom_column', 'slider_columns');
    function slider_columns($name) {
        global $post;
        $slider_title = get_post_meta($post->ID, "slider_title", true);
        $slider_desc = get_post_meta($post->ID, "slider_desc", true);
        $post_featured_image = mt_get_featured_image($post->ID);
        if ($post_featured_image) {
            // HAS A FEATURED IMAGE
          $featured_image = '<img style="width:50px;height:50px;" src="' . $post_featured_image . '" />';
        }
        else {
            $featured_image = 'NO';
            // NO FEATURED IMAGE, SHOW THE DEFAULT ONE
         // $featured_image = '<img src="' . get_bloginfo( 'template_url' ); . '/images/default.jpg" />';
        }
        switch ($name) {
            case 'image':
                echo $featured_image;
                break;
            case 'slider_id':
                echo esc_attr($post->ID);
                break;
            case 'slider_title':
                echo esc_attr($slider_title);
                break;
            case 'slider_desc':
                echo esc_attr($slider_desc);
                break;
        }
    }

// *********************************** WP Admin Customer Custom Columns Display End *************************************//

// *********************************** WP Admin Customer Custom Meta Box Display Start *************************************//

// GET FEATURED IMAGE
function mt_get_featured_image($post_ID) 
{
    $post_thumbnail_id = get_post_thumbnail_id($post_ID);
    if ($post_thumbnail_id) {
        $post_thumbnail_img = wp_get_attachment_image_src($post_thumbnail_id, 'post-thumbnails');
        return $post_thumbnail_img[0];
    }
}

// WP Admin Display Custom Fields Meta Box for Post Type Customer
function display_slider($slider)
{
    global $post;
    $custom = get_post_custom($post->ID);
    $slider_title = $custom["slider_title"][0];
    $slider_desc = $custom["slider_desc"][0];
?>
    
    <style>
        body{background-color: #f1f1f1 !important;}
    </style>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"
          integrity="sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ=="
          crossorigin="anonymous">
<div class="form form-horizontal">
    <div class="form-group">
        <label class="control-label col-sm-3" for="slider_id">Slider ID :</label>
        <div class="col-md-9">
            <input type="text" class="form-control" name="slider_id" value="<?php echo $post->ID; ?>" disabled/>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-3" for="slider_title">Slider Title :</label>
        <div class="col-md-9"><input type="text" class="form-control" name="slider_title" value="<?php echo $slider_title; ?>"></div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-3" for="publisher_id">Slider Description :</label>
        <div class="col-md-9"><input type="text" class="form-control" name="slider_desc" value="<?php echo $slider_desc; ?>" /></div>
    </div>
</div>
<?php 
 }
 function display_meta_slider()
{
    add_meta_box('slider',
        'Create Slider',
        'display_slider',
        'slider', 'normal', 'high'
    );

}